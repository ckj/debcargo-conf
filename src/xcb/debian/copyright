Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xcb
Upstream-Contact: Remi Thebault <remi.thebault@gmail.com>
Source: https://github.com/rust-x-bindings/rust-xcb

Files: *
Copyright:
 2013 James Miller <james@aatch.net>
 2016 Remi Thebault <remi.thebault@gmail.com>
 2016 Thomas Bracht Laumann Jespersen <laumann.thomas@gmail.com>
License: MIT

Files: ./xml/bigreq.xml
Copyright: 2001-2004 Bart Massey, Jamey Sharp, and Josh Triplett
License: MIT-plus-author-restriction

Files: ./xml/composite.xml
Copyright: 2004 Josh Triplett
License: MIT-plus-author-restriction

Files: ./xml/damage.xml
Copyright:
 2004 Josh Triplett
 2007 Jeremy Kolb
License: MIT-plus-author-restriction

Files: ./xml/dpms.xml
Copyright: 2001-2004 Bart Massey, Jamey Sharp, and Josh Triplett
License: MIT-plus-author-restriction

Files: ./xml/dri2.xml
Copyright:
 2005 Jeremy Kolb.
 2009 Intel Corporation
License: MIT-plus-author-restriction

Files: ./xml/dri3.xml
Copyright: 2013 Keith Packard
License: MIT-plus-author-restriction

Files: ./xml/ge.xml
Copyright: 2009 Open Text Corporation
License: MIT-plus-author-restriction

Files: ./xml/glx.xml
Copyright: 2005 Jeremy Kolb.
License: MIT-plus-author-restriction

Files: ./xml/present.xml
Copyright: 2013 Keith Packard
License: MIT-plus-author-restriction

Files: ./xml/randr.xml
Copyright: 2006 Jeremy Kolb, Ian Osgood
License: MIT-plus-author-restriction

Files: ./xml/record.xml
Copyright: 2005 Jeremy Kolb.
License: MIT-plus-author-restriction

Files: ./xml/render.xml
Copyright: 2002-2004 Carl D. Worth, Jamey Sharp, Bart Massey, Josh Triplett
License: MIT-plus-author-restriction

Files: ./xml/res.xml
Copyright: 2006 Jeremy Kolb
License: MIT-plus-author-restriction

Files: ./xml/screensaver.xml
Copyright: 2005 Vincent Torri.
License: MIT-plus-author-restriction

Files: ./xml/shape.xml
Copyright: 2001-2004 Bart Massey, Jamey Sharp, and Josh Triplett.
License: MIT-plus-author-restriction

Files: ./xml/shm.xml
Copyright: 2001-2004 Bart Massey, Jamey Sharp, and Josh Triplett.
License: MIT-plus-author-restriction

Files: ./xml/sync.xml
Copyright: 2004 Mikko Torni and Josh Triplett.
License: MIT-plus-author-restriction

Files: ./xml/xc_misc.xml
Copyright: 2004 Mikko Torni and Josh Triplett.
License: MIT-plus-author-restriction

Files: ./xml/xcb.xsd
Copyright: 2004 Josh Triplett
License: MIT-plus-author-restriction

Files: ./xml/xevie.xml
Copyright: 2004 Josh Triplett
License: MIT-plus-author-restriction

Files: ./xml/xf86dri.xml
Copyright: 2005 Jeremy Kolb.
License: MIT-plus-author-restriction

Files: ./xml/xf86vidmode.xml
Copyright: 2009 Open Text Corporation
License: MIT-plus-author-restriction

Files: ./xml/xfixes.xml
Copyright: 2004 Josh Triplett
License: MIT-plus-author-restriction

Files: ./xml/xinerama.xml
Copyright: 2006 Jeremy Kolb.
License: MIT-plus-author-restriction

Files: ./xml/xinput.xml
Copyright:
 2006 Peter Hutterer
 2013 Daniel Martin
License: MIT-plus-author-restriction

Files: ./xml/xkb.xml
Copyright: 2009 Open Text Corporation
License: MIT-plus-author-restriction

Files: ./xml/xprint.xml
Copyright: 2005 Jeremy Kolb.
License: MIT-plus-author-restriction

Files: ./xml/xproto.xml
Copyright: 2001-2004 Bart Massey, Jamey Sharp, and Josh Triplett
License: MIT-plus-author-restriction

Files: ./xml/xtest.xml
Copyright: 2006 Ian Osgood
License: MIT-plus-author-restriction

Files: ./xml/xv.xml
Copyright: 2006 Jeremy Kolb.
License: MIT-plus-author-restriction

Files: ./xml/xvmc.xml
Copyright: 2006 Jeremy Kolb.
License: MIT-plus-author-restriction

Files: debian/*
Copyright:
 2019-2022 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2019-2022 Ximin Luo <infinity0@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: MIT-plus-author-restriction
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 .
 Except as contained in this notice, the names of the authors or their
 institutions shall not be used in advertising or otherwise to promote the
 sale, use or other dealings in this Software without prior written
 authorization from the authors.
